[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

A mod that adds a lot of compatibility recipes that fixes issues with schematics in the early game for Minecolonies.

Note: Minecolonies is not a requirement to benefit!

Adds in ways to get flowers so you don't have to go bonemeal crazy, also you can get grass and path blocks, and dyes from coral.

Check out this [wiki](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fgitlab.com%252fkreezxil%252fBetter-With-Minecolonies%252f-%252fwikis%252fhome) for everything added by this mod.
